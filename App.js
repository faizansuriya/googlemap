import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TouchableOpacity,
  TextInput
} from "react-native";
// import RNGooglePlaces from "react-native-google-places";
import MapView, { Marker } from "react-native-maps";

class GPlacesDemo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      region: {
        latitude: 37.78825,
        longitude: -122.4324,
        latitudeDelta: 0.015,
        longitudeDelta: 0.0121
      }
    };
  }

  // getInitialState = () => {
  //   return {
  //     region: {
  //       latitude: 37.78825,
  //       longitude: -122.4324,
  //       latitudeDelta: 0.015,
  //       longitudeDelta: 0.0121
  //     }
  //   };
  // };

  // onRegionChange = region => {
  //   this.setState({ region });
  // };

  render() {
    return (
      <MapView
        style={{
          height: "100%",
          width: "100%"
        }}
        region={this.state.region}
        onRegionChange={this.onRegionChange}
      >
        <Marker
          coordinate={this.state.region}
          title={"marker.title"}
          description={"marker.description"}
        />
      </MapView>
    );
  }

  // openSearchModal = () => {
  //   RNGooglePlaces.openPlacePickerModal()
  //     .then(place => {
  //       console.log(place);
  //       this.setState({
  //         address: place.address,
  //         latitude: place.latitude,
  //         longitude: place.longitude
  //       });
  //       // place represents user's selection from the
  //       // suggestions and it is a simplified Google Place object.
  //     })
  //     .catch(error => console.log(error.message)); // error is a Javascript Error object
  // };

  // render() {
  //   return (
  //     <View>
  //       <Text>This is the map view</Text>
  //     </View>
  // <View style={styles.container}>
  //   {!this.state.address ? (
  //     <Button
  //       onPress={this.openSearchModal}
  //       title="Address"
  //       color="black"
  //     />
  //   ) : (
  //     <Button
  //       onPress={this.openSearchModal}
  //       title={this.state.address}
  //       color="black"
  //     />
  //   )}
  // </View>
  //   );
  // }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 30,
    paddingRight: 30,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  input: {
    margin: 15,
    height: 40,
    width: "100%",
    borderColor: "#000000",
    borderWidth: 1
  }
});

export default GPlacesDemo;
